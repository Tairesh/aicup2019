import model
import model.item
import model.custom_data
from debug import Debug
from typing import Optional, Tuple
from utils import get_rectangles_for_tiles, get_tiles_between, tile_neighbours
import geometry

RED = model.ColorFloat(1.0, 0.0, 0.0, 0.5)
YELLOW = model.ColorFloat(1.0, 1.0, 0.0, 0.5)
GREEN = model.ColorFloat(0.0, 1.0, 0.0, 0.5)
BLUE = model.ColorFloat(0.0, 0.0, 1.0, 0.5)

PREFERED_WEAPON = model.WeaponType.ROCKET_LAUNCHER

try_places = ((-1, 0), (0, -1), (0, 1), (1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1), (-2, 0), (0, -2), (0, 2), (2, 0), (-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1), (-2, -2), (-2, 2), (2, -2), (2, 2), (-3, 0), (0, -3), (0, 3), (3, 0), (-3, -1), (-3, 1), (-1, -3), (-1, 3), (1, -3), (1, 3), (3, -1), (3, 1), (-3, -2), (-3, 2), (-2, -3), (-2, 3), (2, -3), (2, 3), (3, -2), (3, 2), (-3, -3), (-3, 3), (3, -3), (3, 3))


class MyStrategy:
    def __init__(self):
        self.unit: Optional[model.Unit] = None
        self.game: Optional[model.Game] = None
        self.debug: Optional[Debug] = None
        self.start_pos: Optional[Tuple[int, int]] = None
        self.swap_pos: Optional[Tuple[int, int]] = None
        self.nearest_enemy: Optional[model.Unit] = None
        self.nearest_weapon: Optional[model.LootBox] = None
        self.nearest_health: Optional[model.LootBox] = None
        self.nearest_rocket: Optional[model.LootBox] = None
        self.nearest_mine: Optional[model.LootBox] = None
        self.danger_bullets = []
        self.danger_mines = []
        self.walls_rectangles = []
        self.danger_rocket_targets = []

        self.rocket_explosion = 3.0
        self.rocket_size = 0.1
        self.current_tiles = {}
        self.last_tiles = {}

    @property
    def current_tile(self):
        return self.current_tiles[self.unit.id]if self.unit.id in self.current_tiles else None

    @property
    def last_tile(self):
        return self.last_tiles[self.unit.id] if self.unit.id in self.last_tiles else None

    def get_place_danger(self, try_place):

        unit_rect = geometry.Rect(try_place.x, try_place.y + self.game.properties.unit_size.y / 2,
                                  self.game.properties.unit_size.x, self.game.properties.unit_size.y)

        tx, ty = int(try_place.x), int(try_place.y)
        if tx < 0 or ty < 0 or tx > len(self.game.level.tiles) - 1 or ty > len(self.game.level.tiles[tx]) - 1:
            count = 99999
        else:
            segment = geometry.LineSegment(try_place, self.unit.position)
            if any(rect.intersect_segment(segment) for rect in self.walls_rectangles):
                count = 99999
            else:
                count = 0
                for bullet in self.danger_bullets:
                    if self.bullet_intersect_unit(bullet, try_place):
                        count += bullet.damage
                        if bullet.weapon_type == model.WeaponType.ROCKET_LAUNCHER:
                            count += 50
                for mine in self.danger_mines:
                    r = mine.explosion_params.radius + 1.0
                    point = min(unit_rect.points, key=lambda p: p.dist_to(self.unit.position))
                    dist = max(abs(mine.position.x - point.x), abs(mine.position.y - point.y))
                    if dist <= r:
                        count += 50 + 1/dist
                for place in self.danger_rocket_targets:
                    point = min(unit_rect.points, key=lambda p: p.dist_to(place))
                    dist = max(abs(place.x - point.x), abs(place.y - point.y))
                    if dist <= self.rocket_explosion + 1.0:
                        count += 50 + 1/dist
        return count

    def init_turn(self, unit: model.Unit, game: model.Game, debug: Debug):
        self.unit = unit
        self.game = game
        self.debug = debug
        self.current_tiles[unit.id] = (int(unit.position.x), int(unit.position.y))

        self.nearest_enemy = None
        enemies = sorted(filter(lambda u: u.player_id != self.unit.player_id, self.game.units), key=lambda u: u.position.dist_to(self.unit.position))
        for enemy in enemies:
            segment = geometry.LineSegment(enemy.position.x,enemy.position.y + 0.9,
                                           unit.position.x, unit.position.y + 0.9)
            if not any(rect.intersect_segment(segment) for rect in self.walls_rectangles):
                self.nearest_enemy = enemy
                break
        if not self.nearest_enemy:
            self.nearest_enemy = min(enemies, key=lambda u: u.position.dist_to(self.unit.position))

        self.nearest_weapon = min(filter(lambda box: isinstance(box.item, model.item.Weapon), self.game.loot_boxes),
                                  key=lambda box: box.position.dist_to(self.unit.position),
                                  default=None)
        danger_left = self.nearest_enemy.position.x < self.unit.position.x
        danger_dist_x = abs(self.nearest_enemy.position.x - self.unit.position.x)
        danger_dist_y = abs(self.nearest_enemy.position.y - self.unit.position.y)
        health_boxes = filter(lambda box: isinstance(box.item, model.item.HealthPack), self.game.loot_boxes)
        if danger_dist_x < 1.5 and danger_dist_y < 2.0:
            health_boxes = filter(lambda box: (box.position.x < unit.position.x and danger_left) or
                                              (box.position.x > unit.position.x and not danger_left), health_boxes)
        self.nearest_health = min(health_boxes,
                                  key=lambda box: box.position.dist_to(self.unit.position),
                                  default=None)
        self.nearest_rocket = min(filter(lambda box: isinstance(box.item, model.item.Weapon)
                                         and box.item.weapon_type == PREFERED_WEAPON,
                                         self.game.loot_boxes),
                                  key=lambda box: box.position.dist_to(self.unit.position),
                                  default=None)
        mines = filter(lambda box: isinstance(box.item, model.item.Mine), self.game.loot_boxes)
        if danger_dist_x < 1.5 and danger_dist_y < 2.0:
            mines = filter(lambda box: (box.position.x < unit.position.x and danger_left) or
                                       (box.position.x > unit.position.x and not danger_left), mines)
        self.nearest_mine = min(mines,
                                key=lambda box: box.position.dist_to(self.unit.position),
                                default=None)

        self.danger_bullets = []
        self.danger_rocket_targets = []
        for bullet in game.bullets:
            if bullet.weapon_type == model.WeaponType.ROCKET_LAUNCHER:
                segment = geometry.LineSegment(bullet.position.x, bullet.position.y,
                                               bullet.position.x+bullet.velocity.x, bullet.position.y+bullet.velocity.y)

                rect = min(filter(lambda wall: wall.intersect_segment(segment), self.walls_rectangles),
                           key=lambda wall: wall.center.dist_to(bullet.position), default=None)
                if rect is not None:
                    point = min(rect.points, key=lambda p: p.dist_to(unit.position))
                    self.danger_rocket_targets.append(point)

            if bullet.player_id == unit.player_id and bullet.weapon_type != model.WeaponType.ROCKET_LAUNCHER:
                continue
            if self.bullet_intersect_unit(bullet, unit.position):
                self.danger_bullets.append(bullet)

        self.danger_mines = []
        for mine in game.mines:
            r = mine.explosion_params.radius + 0.9
            if abs(unit.position.x - mine.position.x) <= r and abs(unit.position.y - mine.position.y) <= r:
                self.danger_mines.append(mine)
        for enemy_unit in filter(lambda u: u.player_id != self.unit.player_id, game.units):
            if enemy_unit.mines > 0 and enemy_unit.on_ground:
                virtual_mine = model.Mine(enemy_unit.player_id, enemy_unit.position, game.properties.mine_size, 0, 0,
                                          game.properties.mine_trigger_radius, game.properties.mine_explosion_params)
                self.danger_mines.append(virtual_mine)

        if self.start_pos is None:
            self.start_pos = unit.position
            self.swap_pos = self.nearest_enemy.position
            self.rocket_explosion = game.properties.weapon_params[model.WeaponType.ROCKET_LAUNCHER].explosion.radius
            self.rocket_size = game.properties.weapon_params[model.WeaponType.ROCKET_LAUNCHER].bullet.size * 0.71

            walls = []
            for i, row in enumerate(game.level.tiles):
                for j, tile in enumerate(row):
                    if tile == model.Tile.WALL:
                        walls.append((i, j))
            self.walls_rectangles = get_rectangles_for_tiles(walls)

    def bullet_intersect_unit(self, bullet: model.Bullet, unit_pos: model.Vec2Double):
        offset = 1.0
        uw, uh = self.game.properties.unit_size.x, self.game.properties.unit_size.y
        ux, uy = unit_pos.x, unit_pos.y + uh/2
        unit_rect = geometry.Rect(ux, uy, uw+offset, uh+offset)
        velocity = geometry.Vector(bullet.velocity.x, bullet.velocity.y)
        a = (bullet.position.x - bullet.velocity.x*0.1, bullet.position.y - bullet.velocity.y*0.1)
        b = (a[0] + velocity.x, a[1] + velocity.y)
        bullet_segment = geometry.LineSegment(*a, *b)
        bullet_to_pos = geometry.LineSegment(*a, ux, uy)
        rectangles = get_rectangles_for_tiles((i, j) for i, j in get_tiles_between(bullet.position, unit_pos) if self.game.level.tiles[i][j] == model.Tile.WALL)
        if self.debug:
            # self.debug.draw(model.custom_data.Line(model.Vec2Float(*a), model.Vec2Float(ux, uy), 0.2, RED))
            # self.debug.draw(model.custom_data.Rect(model.Vec2Float(ux-uw/2, uy), model.Vec2Float(uw, uh), model.ColorFloat(0.0, 1.0, 1.0, 0.2)))
            for rect in rectangles:
                if rect.intersect_segment(bullet_to_pos):
                    self.debug.draw(model.custom_data.Rect(model.Vec2Float(rect.left, rect.bottom), model.Vec2Float(rect.w, rect.h), YELLOW))
        if any(rect.intersect_segment(bullet_to_pos) for rect in rectangles):
            danger = False
        else:
            danger = unit_rect.intersect_segment(bullet_segment)
        if self.debug:
            self.debug.draw(model.custom_data.Line(model.Vec2Float(*a), model.Vec2Float(*b), 0.1, RED if danger else GREEN))
        return danger

    @property
    def my_player(self):
        for player in self.game.players:
            if player.id == self.unit.player_id:
                return player
        return None

    @property
    def enemy_player(self):
        for player in self.game.players:
            if player.id != self.unit.player_id:
                return player
        return None

    @property
    def partner(self) -> Optional[model.Unit]:
        for unit in self.game.units:
            if unit.player_id == self.unit.player_id and unit.id != self.unit.id:
                return unit
        return None

    def get_action(self, unit: model.Unit, game: model.Game, debug: Debug):
        self.init_turn(unit, game, debug)

        target_pos = self.start_pos
        swap_weapon = False

        if unit.weapon is None and self.nearest_weapon is not None:
            target_pos = self.nearest_weapon.position
        elif len(self.danger_bullets):
            counts = []
            for dx, dy in try_places:
                if dy > 0 and unit.jump_state.max_time == 0:
                    count = 999
                else:
                    try_place = model.Vec2Double(unit.position.x + dx, unit.position.y + dy)
                    tx, ty = int(try_place.x), int(try_place.y)
                    if tx < 0 or ty < 0 or tx > len(game.level.tiles)-1 or ty > len(game.level.tiles[tx])-1:
                        count = 999
                    else:
                        # line = get_line((tx, ty), (int(unit.position.x), int(unit.position.y)))
                        # if any(game.level.tiles[i][j] == model.Tile.WALL for i, j in line):
                        #     count = 99
                        segment = geometry.LineSegment(try_place, unit.position)
                        if any(rect.intersect_segment(segment) for rect in self.walls_rectangles):
                            count = 99
                        else:
                            count = 0
                            for bullet in self.danger_bullets:
                                if self.bullet_intersect_unit(bullet, try_place):
                                    count += 1
                counts.append(count)
            min_count = min(counts)
            for i in range(len(counts)):
                if counts[i] == min_count:
                    dx, dy = try_places[i]
                    target_pos = model.Vec2Double(unit.position.x + dx, unit.position.y + dy)
                    break
        elif unit.health <= game.properties.unit_max_health * 0.51 and self.nearest_health is not None:
            target_pos = self.nearest_health.position
        elif unit.weapon.typ != PREFERED_WEAPON and self.nearest_rocket:
            target_pos = self.nearest_rocket.position
            if target_pos.dist_to(unit.position) < 2.0:
                swap_weapon = True
        elif unit.mines < 1 and self.nearest_mine and unit.weapon and unit.weapon.typ == PREFERED_WEAPON:
            target_pos = self.nearest_mine.position
        elif self.nearest_enemy is not None and unit.weapon is not None and (not unit.weapon.fire_timer or unit.weapon.typ != PREFERED_WEAPON) and self.my_player.score <= self.enemy_player.score:
            target_pos = self.nearest_enemy.position
        # elif self.nearest_mine:
        #     target_pos = self.nearest_mine.position
        #     target_pos.y += 0.01
        elif self.nearest_health:
            target_pos = self.nearest_health.position

        tile_left = game.level.tiles[int(target_pos.x) - 1][int(target_pos.y)] if int(target_pos.x) - 1 > 0 else model.Tile.WALL
        tile_right = game.level.tiles[int(target_pos.x) + 1][int(target_pos.y)] if int(target_pos.x) + 1 < len(game.level.tiles) else model.Tile.WALL
        tile_down = game.level.tiles[int(target_pos.x)][int(target_pos.y)-1] if int(target_pos.y) - 1 > 0 else model.Tile.WALL
        if tile_left in {model.Tile.WALL, model.Tile.JUMP_PAD}:
            target_pos.x += 0.5
        elif tile_right in {model.Tile.WALL, model.Tile.JUMP_PAD}:
            target_pos.x -= 0.5

        if self.debug:
            # debug.draw(model.CustomData.Log("Jump time: {}".format(unit.jump_state.max_time)))
            debug.draw(
                model.custom_data.Rect(model.Vec2Float(target_pos.x - 0.5, target_pos.y - 0.5), model.Vec2Float(1.0, 1.0),
                                       model.ColorFloat(0.0, 1.0, 0.0, 0.5)))
        partner_rect = None
        if self.partner:
            partner_rect = geometry.Rect(self.partner.position.x,
                                         self.partner.position.y + game.properties.unit_size.y / 2,
                                         game.properties.unit_size.x,
                                         game.properties.unit_size.y)

        aim = model.Vec2Double(0, 0)
        shoot = False
        if self.nearest_enemy is not None and self.unit.weapon is not None:
            shoot_pos = geometry.Point(unit.position.x, unit.position.y + game.properties.unit_size.y / 2)
            enemy_pos = model.Vec2Float(self.nearest_enemy.position.x,
                                        self.nearest_enemy.position.y + game.properties.unit_size.y / 2)

            aim = model.Vec2Double(enemy_pos.x - shoot_pos.x, enemy_pos.y - shoot_pos.y)
            while aim.length < 0.500001:
                aim.extend(2)

            segment = geometry.LineSegment(shoot_pos, enemy_pos)
            if segment.length <= 2.0:
                shoot = True
            else:
                if self.debug:
                    for rect in self.walls_rectangles:
                        color = model.ColorFloat(1.0, 0.0, 0.0, 0.3) if rect.intersect_segment(segment) else model.ColorFloat(0.0, 0.0, 1.0, 0.3)
                        debug.draw(model.custom_data.Rect(model.Vec2Float(rect.left, rect.bottom), model.Vec2Float(rect.w, rect.h), color))

                    # debug.draw(model.custom_data.Line(model.Vec2Float(segment.x1, segment.y1),
                    #                                   model.Vec2Float(segment.x2, segment.y2),
                    #                                   0.2, model.ColorFloat(1.0, 1.0, 0.0, 0.7)))

                shoot = not any(rect.intersect_segment(segment) for rect in self.walls_rectangles)
                if self.partner and partner_rect.intersect_segment(segment):
                    shoot = False

                if shoot and unit.weapon.typ == model.WeaponType.ROCKET_LAUNCHER:
                    vec = geometry.Vector(aim.x, aim.y).set_length(self.rocket_explosion*1.2)
                    right_vec = geometry.Vector(vec.x, vec.y).rotate(-unit.weapon.spread)
                    left_vec = geometry.Vector(vec.x, vec.y).rotate(unit.weapon.spread)
                    right_vec_normal = geometry.Vector(0, self.rocket_size if enemy_pos.x < shoot_pos.x else -self.rocket_size)
                    left_vec_normal = geometry.Vector(0, -self.rocket_size if enemy_pos.x < shoot_pos.x else self.rocket_size)

                    right_pos = geometry.Point(shoot_pos.x + right_vec.x, shoot_pos.y + right_vec.y)
                    left_pos = geometry.Point(shoot_pos.x + left_vec.x, shoot_pos.y + left_vec.y)
                    right_segment = geometry.LineSegment(shoot_pos + right_vec_normal, right_pos)
                    left_segment = geometry.LineSegment(shoot_pos + left_vec_normal, left_pos)
                    if self.debug:
                        debug.draw(model.custom_data.Line(model.Vec2Float(left_segment.x1, left_segment.y1),
                                                          model.Vec2Float(left_segment.x2, left_segment.y2), 0.1,
                                                          model.ColorFloat(1.0, 1.0, 0.0, 0.7)))
                        debug.draw(model.custom_data.Line(model.Vec2Float(right_segment.x1, right_segment.y1),
                                                          model.Vec2Float(right_segment.x2, right_segment.y2), 0.1,
                                                          model.ColorFloat(1.0, 1.0, 0.0, 0.7)))

                    # if left_pos.square_dist_to(enemy_pos) > self.rocket_explosion or\
                    #         right_pos.square_dist_to(enemy_pos) > self.rocket_explosion:
                    #     shoot = False
                    if any(rect.intersect_segment(right_segment) or rect.intersect_segment(left_segment) for rect in self.walls_rectangles):
                        shoot = False

                    if self.partner and (partner_rect.intersect_segment(right_segment) or partner_rect.intersect_segment(left_segment)):
                        shoot = False

        if target_pos.x > unit.position.x:
            target_pos = model.Vec2Double(target_pos.x + 0.1, target_pos.y)
        elif target_pos.x < unit.position.x:
            target_pos = model.Vec2Double(target_pos.x - 0.1, target_pos.y)

        # debug.draw(model.custom_data.Log(f"speed: {self.unit.jump_state.speed}, mt: {self.unit.jump_state.max_time}"))

        jump = False
        if self.unit.jump_state.speed > 0:
            jump = True

        if self.nearest_enemy.position.dist_to(self.unit.position) < 5.0 and unit.mines > 0:
            jump = False
        elif (target_pos.y > unit.position.y and target_pos.y-unit.position.y > 0.5) or abs(target_pos.x - unit.position.x) > 0.9:
            jump = True
            if self.current_tile != self.last_tile and self.last_tile:
                x, y = self.last_tile
                cx, cy = self.current_tile
                # debug.draw(model.custom_data.Log(f"Changed tile {game.level.tiles[x][y]} to {game.level.tiles[cx][cy]}"))
                if game.level.tiles[x][y] == model.Tile.PLATFORM and cx == x and cy > y:
                    jump = False

        jump_down = False
        if not jump and target_pos.y < unit.position.y-game.properties.unit_fall_speed/60 and abs(target_pos.x - unit.position.x) < 3.0 and unit.position.y - target_pos.y > 1.0:
            jump_down = True

        reload = unit.weapon and unit.weapon.magazine == 0

        if target_pos.x > unit.position.x:
            vel = game.properties.unit_max_horizontal_speed
        elif target_pos.x < unit.position.x:
            vel = -game.properties.unit_max_horizontal_speed
        else:
            vel = 0.0

        next_x = unit.position.x + vel/60
        next_y = unit.position.y
        if jump and unit.jump_state.max_time > 0:
            next_y += game.properties.unit_jump_speed/60
        elif jump_down:
            next_y -= game.properties.unit_fall_speed/60

        danger = self.get_place_danger(geometry.Point(next_x, next_y))
        if danger:
            min_danger = 9999
            min_danger_tp = None
            for dx, dy in try_places:
                try_place = geometry.Point(unit.position.x + dx, unit.position.y + dy)
                tp_danger = self.get_place_danger(try_place)
                if debug:
                    color = GREEN
                    if tp_danger > 100:
                        color = RED
                    elif tp_danger > 0:
                        color = YELLOW
                    debug.draw(model.custom_data.Line(model.Vec2Float(unit.position.x, unit.position.y), model.Vec2Float(try_place.x, try_place.y), 0.1, color))
                if tp_danger < danger and tp_danger < min_danger:
                    min_danger = tp_danger
                    min_danger_tp = (dx, dy)
                    if min_danger == 0:
                        break

            if min_danger < danger and min_danger_tp:
                dx, dy = min_danger_tp
                vel = 9999 if dx > 0 else (-9999 if dx < 0 else 0)
                jump = dy > 0
                jump_down = dy < 0

        plant_mine = False
        dist = self.nearest_enemy.position.dist_to(unit.position)
        if dist < 4.0 and unit.on_ground:
            plant_mine = True

        self.last_tiles[unit.id] = self.current_tile

        if self.debug:
            debug.draw(model.custom_data.Line(model.Vec2Float(unit.position.x, unit.position.y),
                                              model.Vec2Float(next_x, next_y),
                                              0.2, GREEN))
        return model.UnitAction(
            velocity=vel,
            jump=jump,
            jump_down=jump_down,
            aim=aim,
            shoot=shoot,
            reload=reload,
            swap_weapon=swap_weapon,
            plant_mine=plant_mine)
