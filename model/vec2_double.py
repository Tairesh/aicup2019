import math


class Vec2Double:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def extend(self, val):
        self.x *= val
        self.y *= val

    @property
    def length(self):
        return math.hypot(self.x, self.y)

    def dist_to(self, point):
        return math.hypot(self.x-point.x, self.y-point.y)

    @staticmethod
    def read_from(stream):
        x = stream.read_double()
        y = stream.read_double()
        return Vec2Double(x, y)

    def write_to(self, stream):
        stream.write_double(self.x)
        stream.write_double(self.y)

    def __repr__(self):
        return "Vec2Double(" + \
            repr(self.x) + "," + \
            repr(self.y) + \
            ")"
