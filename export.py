from zipfile import ZipFile
import os

ZIP_NAME = 'aicup-python.zip'

os.remove(ZIP_NAME)

main_dir = os.path.dirname(__file__)
model_dir = main_dir + '/model'

with ZipFile(ZIP_NAME, 'w') as myzip:
    for file in os.listdir(main_dir):
        if file != 'export.py' and os.path.isfile(file) and file.endswith('.py'):
            myzip.write(file)
    for file in os.listdir(model_dir):
        if os.path.isfile(model_dir+'/'+file) and file.endswith('.py'):
            myzip.write('model/'+file)
