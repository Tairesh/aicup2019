import numpy as np
import utils
from typing import Tuple


class Point:

    def __init__(self, x, y=None):
        if y is None:
            x, y = x.x, x.y
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return  Point(self.x - other.x, self.y - other.y)

    def dist_to(self, point):
        return np.sqrt((self.x-point.x)**2 + (self.y-point.y)**2)

    def square_dist_to(self, point):
        return min(abs(self.x - point.x), abs(self.y - point.y))

    def __repr__(self):
        return "Point(" + \
            repr(self.x) + "," + \
            repr(self.y) + \
            ")"


class Vector(Point):

    @property
    def length(self):
        return np.sqrt(self.x**2 + self.y**2)

    @property
    def phi(self):
        return np.arctan2(self.y, self.x)

    def __mul__(self, other):
        self.x *= other
        self.y *= other
        return self

    def rotate(self, angle):
        self.x, self.y = utils.rotate_vector(self.x, self.y, angle)
        return self

    def negate(self):
        self.x *= -1
        self.y *= -1
        return self

    def normalize(self):
        """
        sets length to 1
        :return:
        """
        ll = self.length
        if ll == 0.0:
            raise Exception("Can't normalize zero-width vector.")
        self.x /= ll
        self.y /= ll
        return self

    def set_phi(self, angle):
        ll = self.length
        if ll == 0.0:
            raise Exception("Can't normalize zero-width vector.")
        self.x = np.cos(angle) * ll
        self.y = np.sin(angle) * ll
        return self

    def set_length(self, length):
        ll = self.length
        if ll == 0.0:
            raise Exception("Can't normalize zero-width vector.")
        return self * (length / ll)


class LineSegment:

    def __init__(self, x1, y1, x2=None, y2=None):
        if x2 is None or y2 is None:
            x1, y1, x2, y2 = x1.x, x1.y, y1.x, y1.y
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.length = np.hypot(x2-x1, y2-y1)

    def set_length(self, length):
        vec = Vector(self.x2-self.x1, self.y2-self.y1)
        vec.set_length(length)
        self.x2 = self.x1 + vec.x
        self.y2 = self.y1 + vec.y
        return self

    @property
    def point1(self) -> Point:
        return Point(self.x1, self.y1)

    @property
    def point2(self) -> Point:
        return Point(self.x2, self.y2)

    def __add__(self, other):
        self.x1 += other.x
        self.x2 += other.x
        self.y1 += other.y
        self.y2 += other.y
        return self

    def __sub__(self, other):
        self.x1 -= other.x
        self.x2 -= other.x
        self.y1 -= other.y
        self.y2 -= other.y
        return self

    def intersect_segment(self, segment) -> bool:
        def ccw(a, b, c):
            return (c.y - a.y) * (b.x - a.x) > (b.y - a.y) * (c.x - a.x)

        p1, p2, p3, p4 = self.point1, self.point2, segment.point1, segment.point2
        return ccw(p1, p3, p4) != ccw(p2, p3, p4) and ccw(p1, p2, p3) != ccw(p1, p2, p4)


class Rect:

    def __init__(self, x, y, w=None, h=None):
        """
        x, y is the center
        :param x:
        :param y:
        :param w:
        :param h:
        """
        if w is None:
            x, y, w, h = x.x, x.y, y.x, y.y
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    @property
    def left_bottom(self) -> Point:
        return Point(self.left, self.bottom)

    @property
    def left_top(self) -> Point:
        return Point(self.left, self.top)

    @property
    def right_bottom(self) -> Point:
        return Point(self.right, self.bottom)

    @property
    def right_top(self) -> Point:
        return Point(self.right, self.top)

    @property
    def center(self) -> Point:
        return Point(self.x, self.y)

    @property
    def points(self) -> Tuple[Point, Point, Point, Point]:
        return self.left_bottom, self.left_top, self.right_bottom, self.right_top

    @property
    def middle_bottom(self) -> Point:
        return Point(self.x, self.bottom)

    @property
    def left_segment(self) -> LineSegment:
        return LineSegment(self.left_bottom, self.left_top)

    @property
    def right_segment(self) -> LineSegment:
        return LineSegment(self.right_bottom, self.right_top)

    @property
    def top_segment(self) -> LineSegment:
        return LineSegment(self.right_top, self.left_top)

    @property
    def bottom_segment(self) -> LineSegment:
        return LineSegment(self.left_bottom, self.right_bottom)

    @property
    def left(self):
        return self.x - self.w/2

    @property
    def right(self):
        return self.x + self.w/2

    @property
    def top(self):
        return self.y + self.h/2

    @property
    def bottom(self):
        return self.y - self.h/2

    INSIDE = 0
    LEFT = 1
    RIGHT = 2
    BOTTOM = 4
    TOP = 8

    def compute_point(self, x: [Point, float], y=None):
        if y is None:
            x, y = x.x, x.y

        code = self.INSIDE
        if x < self.left:
            code |= self.LEFT
        elif x > self.right:
            code |= self.RIGHT
        if y < self.bottom:
            code |= self.BOTTOM
        elif y > self.top:
            code |= self.TOP

        return code

    def intersect_segment_stupid(self, segment) -> bool:
        return self.left_segment.intersect_segment(segment) or self.right_segment.intersect_segment(segment) or \
                self.top_segment.intersect_segment(segment) or self.bottom_segment.intersect_segment(segment) or \
                (self.left <= segment.point1.x <= self.right and self.bottom <= segment.point1.y <= self.top and
                 self.left <= segment.point2.x <= self.right and self.bottom <= segment.point2.y <= self.top)

    def intersect_segment(self, segment) -> bool:
        p1 = segment.point1
        p2 = segment.point2
        code1 = self.compute_point(p1)
        code2 = self.compute_point(p2)

        while True:
            if code1 == 0 and code2 == 0:
                return True
            elif code1 & code2:
                return False
            else:
                code = code1 if code1 else code2
                x, y = 0, 0
                if code & self.TOP:
                    x = p1.x + (p2.x - p1.x) * (self.top - p1.y) / (p2.y - p1.y)
                    y = self.top
                elif code & self.BOTTOM:
                    x = p1.x + (p2.x - p1.x) * (self.bottom - p1.y) / (p2.y - p1.y)
                    y = self.bottom
                elif code & self.RIGHT:
                    y = p1.y + (p2.y - p1.y) * (self.right - p1.x) / (p2.x - p1.x)
                    x = self.right
                elif code & self.LEFT:
                    y = p1.y + (p2.y - p1.y) * (self.left - p1.x) / (p2.x - p1.x)
                    x = self.left

                if code == code1:
                    p1 = Point(x, y)
                    code1 = self.compute_point(p1)
                else:
                    p2 = Point(x, y)
                    code2 = self.compute_point(p2)
                
    def intersect_rect(self, rect) -> bool:
        # If one rectangle is on left side of other
        if self.left > rect.right or rect.left > self.right:
            return False
        # If one rectangle is above other
        if self.top < rect.bottom or rect.top < self.bottom:
            return False
        return True



