import numpy as np
import geometry


def get_line(start, end):
    """Bresenham's Line Algorithm
    Produces a list of tuples from start and end

    >>> points1 = get_line((0, 0), (3, 4))
    >>> points2 = get_line((3, 4), (0, 0))
    >>> assert(set(points1) == set(points2))
    >>> print points1
    [(0, 0), (1, 1), (1, 2), (2, 3), (3, 4)]
    >>> print points2
    [(3, 4), (2, 3), (1, 2), (1, 1), (0, 0)]
    """
    # Setup initial conditions
    x1, y1 = start
    x2, y2 = end
    dx = x2 - x1
    dy = y2 - y1

    # Determine how steep the line is
    is_steep = abs(dy) > abs(dx)

    # Rotate line
    if is_steep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2

    # Swap start and end points if necessary and store swap state
    swapped = False
    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
        swapped = True

    # Recalculate differentials
    dx = x2 - x1
    dy = y2 - y1

    # Calculate error
    error = int(dx / 2.0)
    ystep = 1 if y1 < y2 else -1

    # Iterate over bounding box generating points between start and end
    y = y1
    points = []
    for x in range(x1, x2 + 1):
        coord = (y, x) if is_steep else (x, y)
        points.append(coord)
        error -= abs(dy)
        if error < 0:
            y += ystep
            error += dx

    # Reverse the list if the coordinates were swapped
    if swapped:
        points.reverse()
    return points


class cached_property(object):
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls=None):
        result = instance.__dict__[self.func.__name__] = self.func(instance)
        return result


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return rho, phi


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y


def rotate_vector(x, y, angle):
    cos = np.cos(angle)
    sin = np.sin(angle)
    return x * cos - y * sin, x * sin + y * cos


def get_tiles_between(pos1, pos2):
    min_x = min(int(pos1.x), int(pos2.x)) - 1
    max_x = max(int(pos1.x), int(pos2.x)) + 1
    min_y = min(int(pos1.y), int(pos2.y)) - 1
    max_y = max(int(pos1.y), int(pos2.y)) + 1
    tiles = []
    x = min_x
    while x <= max_x:
        y = min_y
        while y <= max_y:
            tiles.append((x, y))
            y += 1
        x += 1
    return tiles


def get_rectangles_for_tiles(tiles):
    rectangles = []
    for x, y in tiles:
        rect = geometry.Rect(x+0.5, y+0.5, 1.2, 1.2)
        rectangles.append(rect)
    return rectangles


NEIGHBOURS_OFFSET = ((0, 1), (0, -1), (1, 0), (-1, 0))


def tile_neighbours(x, y):
    return [(x+dx, y+dy) for dx, dy in NEIGHBOURS_OFFSET]
