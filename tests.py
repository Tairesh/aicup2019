from geometry import LineSegment, Rect, Point
import random
import unittest
import time


class TestRectangles(unittest.TestCase):

    def test_creating(self):
        rect = Rect(1.5, 1.5, 1.0, 1.0)
        self.assertAlmostEqual(rect.left, 1.0)
        self.assertAlmostEqual(rect.right, 2.0)
        self.assertAlmostEqual(rect.top, 2.0)
        self.assertAlmostEqual(rect.bottom, 1.0)

    def test_points_computing(self):
        rect = Rect(1.5, 1.5, 1.0, 1.0)
        self.assertEqual(rect.compute_point(Point(0.5, 1.5)), Rect.LEFT)
        self.assertEqual(rect.compute_point(Point(1.5, 1.5)), Rect.INSIDE)
        self.assertEqual(rect.compute_point(Point(2.5, 1.5)), Rect.RIGHT)
        self.assertEqual(rect.compute_point(Point(0.5, 2.5)), Rect.LEFT | Rect.TOP)

        segment = LineSegment(0.5, 2.5, 1.4, 2.3)
        code1 = rect.compute_point(segment.point1)
        code2 = rect.compute_point(segment.point2)
        self.assertEqual(code1, Rect.LEFT | Rect.TOP)
        self.assertEqual(code2, Rect.TOP)
        self.assertFalse(rect.intersect_segment(segment))
        self.assertFalse(rect.intersect_segment_stupid(segment))

        segment = LineSegment(0.5, 1.5, 2.2, 2.3)
        self.assertTrue(rect.intersect_segment(segment))
        self.assertTrue(rect.intersect_segment_stupid(segment))

    def test_time(self):
        rectangles = []
        segments = []
        for i in range(100):
            x, y, w, h = random.random()*100, random.random()*100, random.random()*50+10, random.random()*50+10
            rect = Rect(x, y, w, h)
            rectangles.append(rect)
        for i in range(1000):
            x1, y1, x2, y2 = random.random()*100, random.random()*100, random.random()*100, random.random()*100
            segment = LineSegment(x1, y1, x2, y2)
            segments.append(segment)

        results = {}
        start_time = time.time()
        for rect in rectangles:
            results[rect] = {}
            for segment in segments:
                results[rect][segment] = rect.intersect_segment(segment)
        end_time = time.time()
        print(f"cohen-sutherland: {end_time-start_time} sec")
        results2 = {}
        start_time = time.time()
        for rect in rectangles:
            results2[rect] = {}
            for segment in segments:
                results2[rect][segment] = rect.intersect_segment_stupid(segment)
        end_time = time.time()
        print(f"stupid: {end_time-start_time} sec")

        for key in results:
            for key2 in results[key]:
                self.assertEqual(results[key][key2], results2[key][key2], f"{key.x, key.y, key.w, key.h} {key2.x1, key2.y1, key2.x2, key2.y2}")


if __name__ == '__main__':
    unittest.main()
